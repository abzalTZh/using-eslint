const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");
const { template } = require("lodash");
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
    entry: {
        email: './scripts/email-validator.js',
        joinUs: './scripts/join-us-section.js',
        main: './scripts/main.js'
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: '[name].bundle.js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'index.html'
        }),
        new CopyPlugin({
            patterns: [
                { from: path.resolve(__dirname, "./assets/images"), to: path.resolve(__dirname, "dist/assets/images") }
            ]
        })],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.css$/,
                use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader' }
                ]
            },
            {
                test: /\.(png|jpg)$/i,
                type: 'asset/resource'
            }
        ]
    }
};