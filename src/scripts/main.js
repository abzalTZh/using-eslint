import { SectionCreater } from './join-us-section.js';
import { validate } from './email-validator';
import '../styles/style.css';
import '../styles/normalize.css';

let newSection = new SectionCreater; // eslint-disable-line prefer-const

window.addEventListener('load', newSection.create('advanced'), false);

let form = document.querySelector('form.app-form'); // eslint-disable-line prefer-const
// eslint-disable-next-line max-len
let email = document.querySelector('input.app-form__email-input'); // eslint-disable-line prefer-const

form.addEventListener('submit', () => {
  alert(validate(email.value));
});