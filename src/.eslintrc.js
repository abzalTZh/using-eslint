module.exports = {
  'env': {
    'browser': true,
    'es2021': true
  },
  'extends': 'google',
  'overrides': [
  ],
  'parserOptions': {
    'ecmaVersion': 'latest',
    'sourceType': 'module'
  },
  'rules': {
    'linebreak-style': 0,
    'require-jsdoc': ['error', {
      'require': {
        'FunctionDeclaration': false,
        'MethodDefinition': false,
        'ClassDeclaration': false,
        'ArrowFunctionExpression': false,
        'FunctionExpression': false
      }
    }],
    'max-len': ['error', {
      'code': 100
    }],
    'object-curly-spacing': ['error', 'always'],
    'eol-last': ['error', 'never'],
    'comma-dangle': ['error', 'never'],
    'space-before-function-paren': ['error', 'always']
  }
};